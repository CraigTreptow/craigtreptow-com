# craigtreptow-com

## Docker

### Developing

- Edit files
- Rebuild container: `docker build --tag craigtreptow-com .`
- Run locally: `docker run -p 80:4567 craigtreptow-com`

#### Run a command in a container from the image

- `docker build --tag craigtreptow-com . && docker run --rm --name craigtreptow-com-web craigtreptow-com ls -ltr`
- `docker build --tag craigtreptow-com . && docker run -p 80:4567 --rm --name craigtreptow-com-web craigtreptow-com

### Deploying

This depends on the existance of `heroku.yml`.  

Given these remotes:

```
heroku  https://git.heroku.com/craigtreptow-com.git (fetch)
heroku  https://git.heroku.com/craigtreptow-com.git (push)
origin  git@gitlab.com:CraigTreptow/craigtreptow-com.git (fetch)
origin  git@gitlab.com:CraigTreptow/craigtreptow-com.git (push)
```
We can now git push to Heroku and when Heroku sees the `heroku.yml` file, it will build a Docker image from the Dockerfile and then start the container.

So, deploying becomes:

1. Commit changes as normal and push to repo: `git push origin`
1. deploy to heroku: `git push heroku`
