require "sinatra"

set :bind, '0.0.0.0'

get "/index" do
  erb :index
end

get "/about" do
  erb :about
end

get "/learning" do
  erb :learning
end

get "/resume" do
  erb :resume
end

get "/projects" do
  erb :projects
end

get "/social" do
  erb :social
end

get '/*' do
  erb :index
end
